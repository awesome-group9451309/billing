
FROM maven:3.8.3-openjdk-17 AS build
WORkDIR /app
COPY . /app/
RUN mvn clean package

FROM openjdk:17-alpine
WORkDIR /app
COPY --from=build /app/target/*.jar /app/app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]
